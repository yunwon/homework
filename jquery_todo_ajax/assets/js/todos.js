var todo = {
	//toggle 
	toggle: function(){
		$("#plus").click(function(){
			$("input").toggle();
		})
	},
	getTodo: function(){
		$(document).ready(function(){
           $.ajax({
	        	url: 'http://139.59.230.182:3002/tasks', 
	        	dataType: 'json',
	        	type: 'get', 
	        	success:        	
	        		$.each(response, function(key, value){
				    //append new list with new value at the end of ul
				    console.log(key, value);
			    	("ul").append('<li data-id="' + _id.value + '"><span class="bin"><i class="fa fa-trash-o"></i></span><span id="data">' 
			    	          + name.value+ '</span></li>');
					})//each
		    });//ajax;			
		});
	},//todo.getTodo*/

	//add
	addTodo: function(){
        //get the value
	$('#addTodo').keypress(function(e) {
		if (e.keyCode === 13){ //13은 Enter
           $.ajax({
	        	url: 'http://139.59.230.182:3002/tasks', 
	        	dataType: 'json',
	        	type: 'post', 
	        	data: { name : $('#addTodo').val() },
	        	success: 
	        	function(data){
				    //append new list with new value at the end of ul
			    	$("ul").append('<li data-id="' + data._id + '"><span class="bin"><i class="fa fa-trash-o"></i></span><span id="data">' 
			    		            + data.name + '</span></li>');
			    	$('#addTodo').val('');
				}
		    });//ajax;
       };//if
	}); //keypress

	}, //todo.addTodo

	update: function(){	
		//li값 클릭 시
		$("ul").on("click", "li", function(){

			var li = $(this);
			var id = li.data('id');
		    //console.log(id);
		    //console.log(li.text());

			$.ajax({
				url: "http://139.59.230.182:3002/tasks/" + id,  
			    dataType: 'json', 
			    type: 'PUT',  
			    data: {
					    "_id": id, //cannot be changed.
					    "created_at": Date(),
					    "status": "completed",
					    "name": li.text(),
					  },
			    success: function(data){
                    //console.log(data.name);
					// li 값이 complete 됨. (gray선이 생김)
					li.toggleClass("completed");
			    }
			});
		});
	},

	//delete
	delete: function(){

		//span 값을 클릭하면
		$("#todoList").on("click", ".bin", function(e) {
			var li = $(this).parent();
			var id = li.data('id');

			$.ajax({
	        	url: "http://139.59.230.182:3002/tasks/" + id,  
	        	dataType: 'json',
	        	type: 'DELETE', 
	        	success: 
	        	function(data){
	        		li.remove();
			    	alert(data.message);
			    //	if(li.parent() == $("#editTodo")){
			    //		$("#editTodo").attr("placeholder","Add New Todo");
			    //	}
				}
		    });//ajax;

		// event bubble 현상을 막아주는 역할.
		e.stopPropagation();
	});
   },

	//init
	init: function(){
            todo.addTodo();
        	todo.update();
			todo.delete();
			todo.toggle();
		}

}
todo.init();



